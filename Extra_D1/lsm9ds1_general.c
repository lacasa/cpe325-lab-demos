/*******************************************************************************
 * Copyright 2019 Igor Semenov (is0031@uah.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *******************************************************************************/
#include <stdint.h>
#include "i2c.h"
#include "lsm9ds1_general.h"

/**
 * Reads a piece of memory of LSM9DS1 device in the direct order:
 * a lower address of LSM9DS1 goes to a lower address of the buffer
 * @param deviceAddr is the address of LSM9DS1 device
 * @param registerAddr address of a register in LSM9DS1 to start reading from
 * @param buffer is the buffer to read to
 * @param length is the length of the buffer
 */
void LSM9DS1_readMemory(uint8_t deviceAddr, uint8_t registerAddr, uint8_t* buffer, size_t length) {
    
    // Set the address of the register that we will be reading from
    I2C_startWriting(deviceAddr);
    I2C_write(registerAddr, false);
    
    // Read memory of LSM9DS1 byte by byte in the direct oder:
    // lower address of LSM9DS1 goes to lower address of the buffer
    I2C_startReading(deviceAddr);
    for(uint_fast8_t i = 0; i < length; i++)
        buffer[i] = I2C_read(i == (length - 1));
}

/**
 * Reads a piece of memory of LSM9DS1 device in the reversed order:
 * lower address of LSM9DS1 goes to higher address of the buffer
 * @param deviceAddr is the address of LSM9DS1 device
 * @param registerAddr address of a register in LSM9DS1 to start reading from
 * @param buffer is the buffer to read to
 * @param length is the length of the buffer
 */
void LSM9DS1_readMemoryReversed(uint8_t deviceAddr, uint8_t registerAddr, uint8_t* buffer, size_t length) {

    // Set the address of the register that we will be reading from
    I2C_startWriting(deviceAddr);
    I2C_write(registerAddr, false);

    // Read memory of LSM9DS1 byte by byte in the reversed oder:
    // a lower address of LSM9DS1 goes to a higher address of the buffer
    I2C_startReading(deviceAddr);
    for(int_fast8_t i = length; i >= 0; i--)
        buffer[i] = I2C_read(i == 0);
}

/**
 * Writes a piece of memory of LSM9DS1 device
 * @param deviceAddr is the address of LSM9DS1 device
 * @param registerAddr address of a register in LSM9DS1 to start writing to
 * @param buffer is the source of the data to write
 * @param length is the length of the buffer
 */
void LSM9DS1_writeMemory(uint8_t deviceAddr, uint8_t registerAddr, uint8_t* buffer, size_t length) {
    
    // Set the address of the register that we will be writing to
    I2C_startWriting(deviceAddr);
    I2C_write(registerAddr, false);
    
    // Write memory of LSM9DS1 byte by byte
    for(uint_fast8_t i = 0; i < length; i++)
        I2C_write(buffer[i], i == (length - 1));
}
