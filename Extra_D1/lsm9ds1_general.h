/*******************************************************************************
 * Copyright 2019 Igor Semenov (is0031@uah.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *******************************************************************************/
#pragma once

#include <stdint.h>
#include <stddef.h>

void LSM9DS1_readMemory(uint8_t deviceAddr, uint8_t registerAddr, uint8_t* buffer, size_t length);
void LSM9DS1_readMemoryReversed(uint8_t deviceAddr, uint8_t registerAddr, uint8_t* buffer, size_t length);
void LSM9DS1_writeMemory(uint8_t deviceAddr, uint8_t registerAddr, uint8_t* buffer, size_t length);
