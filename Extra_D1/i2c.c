/*******************************************************************************
 * Copyright 2019 Igor Semenov (is0031@uah.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *******************************************************************************/
#include <msp430.h>
#include "i2c.h"

/**
 * Note: all page references in this file refer to the following document:
 * www.ti.com/lit/ug/slau056l/slau056l.pdf
 */

/**
 * Initializes I2C interface. Sets SMCLK as the clock source for UCB module
 * @param clockDivider determines clock value, which is SMCLK / clockDivider
*/
void I2C_setup(uint16_t clockDivider) {
    
    // Configuring and reconfiguring the USCI module should be done when
    // UCSWRST is set to avoid unpredictable behavior (see page 616).
    UCB0CTL1 |= UCSWRST;
    
    // P3.1 and P3.2 (SDA and SCL) will be controlled by USCI_B0
    P3SEL |= BIT1 | BIT2;

    // The USCI module is configured as an I2C master by selecting the I2C mode
    // with UCMODEx = 11 and UCSYNC = 1 and setting the UCMST bit (see page 625).
    UCB0CTL0 |= UCMST | UCMODE_3 | UCSYNC;
    
    // Use SMCLK as the clock source for the USCI module
    UCB0CTL1 |= UCSSEL_2;
    
    // Set clock divider
    UCB0BR0 = (clockDivider >> 0) & 0xFF;
    UCB0BR1 = (clockDivider >> 8) & 0xFF;

    // Clearing UCSWRST releases the USCI for operation.
    UCB0CTL1 &= ~UCSWRST;
}

/*
 * Sends the stop sequence and waits until it is over
 */
static inline void I2C_stop() {

    // Generate the stop condition
    UCB0CTL1 |= UCTXSTP;

    // Wait until stop condition is sent
    while((UCB0CTL1 & UCTXSTP) != 0);
}

/**
 * Sends the start sequence for further data reading
 * @param slaveAddress is the address of slave device
*/
void I2C_startReading(uint8_t slaveAddress) {
    
    // Set slave address
    UCB0I2CSA = slaveAddress;

    // Set the transmitter mode
    UCB0CTL1 &= ~UCTR;

    // Send the start condition
    UCB0CTL1 |= UCTXSTT;
}

/**
 * Gets a byte from the slave device
 * @param isLast should be true if you want to read the last byte of a sequence
 * @return byte that was read
*/
uint8_t I2C_read(bool isLast) {

    // Make sure slave has accepted the address before starting to read
    while((UCB0CTL1 & UCTXSTT) != 0);
    
    // Stop in advance if this is the last byte of a sequence
    if (isLast) I2C_stop();

    // Wait until the RX buffer is ready to read
    while((IFG2 & UCB0RXIFG) == 0);

    // Return the byte received from the slave
    return UCB0RXBUF;
}

/**
 * Sends the start sequence for further data writing
 * @param slaveAddress is the address of slave device
*/
void I2C_startWriting(uint8_t slaveAddress) {

    // Set slave address
    UCB0I2CSA = slaveAddress;

    // Send start condition
    UCB0CTL1 |= UCTXSTT | UCTR;
}

/**
 * Sends a byte to the slave device
 * @param byte is the data to write
 * @param isLast should be true if you want to write the last byte of a sequence
*/
void I2C_write(uint8_t byte, bool isLast) {
    
    // Wait until TX buffer is ready
    while((IFG2 & UCB0TXIFG) == 0);

    // Start transmission of the byte
    UCB0TXBUF = byte;
    
    // Make sure transmission has started
    while((UCB0CTL1 & UCTXSTT) != 0);

    // Stop if this is the last byte of a sequence
    if (isLast) I2C_stop();
}
