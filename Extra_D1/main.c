/*------------------------------------------------------------------------------
 * File:        main.c
 * Function:    Reads data from the accelerometer of LSM9DS1 module
 * Description: The program uses libraries implemented in files i2c.c,
 *              lsm9ds1_general.c and lsm9ds1_accel.c to configure the
 *              accelerometer and read acceleration values for axes X, Y and Z.
 *              After that it calculates the net acceleration and compensates
 *              for g produced by Earth. This value is called force in this
 *              example, though it is actually force/mass. All four values are
 *              written to a data structure called Packet_t which is sent to
 *              UAH Serial App by UART. 
 * 
 * Clocks:      ACLK = LFXT1 = 32768Hz, MCLK = SMCLK = DCO = ~1MHz
 *              An external watch crystal between XIN & XOUT is required for ACLK
 *
 * Directions: 
 *              1. Disconnect the first two jumpers of H1 as shown below:
 *                       _______
 *                     1|       |2
 *                     3|       |4
 *                     5|-------|6
 *                     7|-------|8
 *                      |_______|
 *                         H1
 *              
 *              2. Start UAH Serial App and load config file UahSerialAppSettings.dat.
 *                 Choose baud rate of 115200, no parity, one stop bit and 8 data bits.
 *                 Check "Enable Chart", choose appropriate COM port and click "Connect".
 *              3. Load the program to MSP430 and let it stop at the beginning
 *                 of the main function.
 *              4. Connect power and data wires to LSM9DS1 as shown below:
 *                                                 _________________
 *                                             /|\|              XIN|-
 *                                              | |                 | 32kHz xtal
 *                                              --|RST          XOUT|-
 *                                                |                 |
 *                         _____________          |                 |
 *                        |             |         |                 |
 *                VCC ----|VCC       SDA|---------|P3.1/UCB0SDA     |
 *                GND ----|GND       SCL|---------|P3.2/UCB0SDL     |
 *                        |_____________|         |                 |
 *                             Slave                     Master
 *                            LSM9DS1                 MSP430FG4618
 *
 *              5. Resume program execution and observe the graph in UAH Serial App
 *              
 *              Note: if you experience any problems with connection, completely
 *                    disconnect the sensor and repeat steps 3-5. You may need to do
 *                    that because LSM9DS1 module does not have a reset pin, so by
 *                    doing that you can make sure that the module is reset before
 *                    any communication attempts are made.
 *              Note: Some of the modules are faulty. When you connect a faulty one,
 *                    the whole Experimenter's board looses power. You can see that if
 *                    the Power LED on the debugger is turned off. Ask your
 *                    instructor to replace the LSM9DS1 module in this case.
 *
 * Author: Igor Semenov (is0031@uah.edu)
 * Date:   February 2019
 *------------------------------------------------------------------------------*/
#include <msp430xG46x.h>
#include <math.h>
#include "i2c.h"
#include "lsm9ds1_accel.h"

/**
 * Structure to keep the content of UAH Serial App packet
 */
typedef struct __attribute__((packed)) {
    uint8_t header;
    Accel_Data_t axes;
    float force;
} Packet_t;

/*
 * Configures UART to communicate at baud rate of 19200
 */
void UART_setup() {

    P2SEL |= BIT4 | BIT5;          // Set UC0TXD and UC0RXD to transmit and receive data
    UCA0CTL1 |= UCSWRST;           // Software reset
    UCA0CTL0 = 0;                  // USCI_A0 control register
    UCA0CTL1 |= UCSSEL_2;          // Clock source SMCLK - 1048576 Hz
    UCA0BR0 = 9;                   // Baud rate - 1048576 Hz / 115200
    UCA0BR1 = 0;
    UCA0MCTL = 0x02;               // Modulation
    UCA0CTL1 &= ~UCSWRST;          // Software reset
}

/*
 * Sends a byte via UART
 * @param byte is the byte to be sent
 */
void inline UART_sendByte(uint8_t byte) {

    while(!(IFG2 & UCA0TXIFG));  // Wait for previous character to transmit
    UCA0TXBUF = byte;            // Put character into tx buffer
}

/*
 * Sends a packet via UART
 * @param packet is the packet to be sent
 */
void UART_sendPacket(Packet_t* packet) {

    // Convert our packet to array of bytes
    uint8_t* data = (uint8_t*)packet;

    // Send data byte by byte
    for(uint_fast8_t i = 0; i < sizeof(Packet_t); i++)
        UART_sendByte(data[i]);
}

/*
 * Program's entry point
 */
void main(void) {

    // Disable the watchdog timer
    WDTCTL = WDTPW | WDTHOLD;

    // Initialize UART
    UART_setup();

    // Initialize I2C to work at 1048576/26 ~ 40 KHz
    I2C_setup(26);

    // Test the accelerometer. If it hangs in this line,
    // something is wrong with I2C connection
    while(!Accel_test());

    // Configure the accelerometer.
    // To learn more about these and other possible
    // parameters, right-click on a setting (ex: ACCEL_CUTOFF_FREQ_DR)
    // and choose 'open declaration'
    Accel_Config_t config;
    config.dataRate = ACCEL_DATA_RATE_476; // Sensor will produce 476 samples/sec
    config.scale = ACCEL_SCALE_2; // 2g is the maximum acceleration in this mode
    config.cutoffFreq = ACCEL_CUTOFF_FREQ_DR; // Anti-aliasing filter cuttoff freq depends on data rate
    config.decimation = ACCEL_DECIMATION_NONE; // We will not skip samples
    config.digitalFilter = ACCEL_DFILTER_400; // Signal looks smoother with this option
    config.xEnable = true;
    config.yEnable = true;
    config.zEnable = true;
    Accel_setup(&config);

    // Allocate a data structure to send data to UAH serial app
    Packet_t packet;

    // Set header of the packet.
    // Read the manual for UAH serial app to learn more
    packet.header = 0x55;

    while(1) {

        // Wait until new data is available
        while(!Accel_isDataAvailable());

        // Read acceleration values
        Accel_readData(&config, &packet.axes);

        // Calculate the value of force
        // (it is actually the force divided by the mass)
        // Calculate the value of force
        // (it is actually the force divided by the mass)
        packet.force = packet.axes.x * packet.axes.x;
        packet.force += packet.axes.y * packet.axes.y;
        packet.force += packet.axes.z * packet.axes.z;
        packet.force = sqrt(packet.force) - 1; // -1 to compensate for g of Earth

        // Send the packet to the serial app
        UART_sendPacket(&packet);
    }
}
