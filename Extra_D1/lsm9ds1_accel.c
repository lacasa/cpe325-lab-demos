/*******************************************************************************
 * Copyright 2019 Igor Semenov (is0031@uah.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *******************************************************************************/

/**
 * Note: all page references in this file refer to the following document:
 * https://www.st.com/resource/en/datasheet/DM00103319.pdf
 */

#include <stdbool.h>
#include "lsm9ds1_general.h"
#include "lsm9ds1_accel.h"

#define DEVICE_ADDR        0x6B
#define OUT_X_XL_ADDR      0x28
#define WHO_I_AM_ADDR      0x0F
#define CTRL_REG5_XL_ADDR  0x1F
#define CTRL_REG6_XL_ADDR  0x20
#define CTRL_REG7_XL_ADDR  0x21
#define STATUS_REG_ADDR    0x27

#define WHO_I_AM_VALUE 0x68
#define XLDA_BIT_MASK  0x01

/**
 * See Table 65 on page 51 for more detail
 */
typedef struct __attribute__((packed)) {
    // Starting from LSB
    unsigned int NA:3; // Not used bits
    unsigned int Xen_XL:1; // Accelerometer's X-axis output enable
    unsigned int Yen_XL:1; // Accelerometer's Y-axis output enable
    unsigned int Zen_XL:1; // Accelerometer's Z-axis output enable
    unsigned int DEC:2; // Decimation of acceleration data on OUT REG and FIFO
} CTRL_REG5_XL_t;

/*
 * See Table 67 on page 52 for more detail
 */
typedef struct __attribute__((packed)) {
    unsigned int BW_XL:2; // Anti-aliasing filter bandwidth selection
    unsigned int BW_SCAL_ODR:1; // Bandwidth selection
    unsigned int FS_XL:2; // Accelerometer full-scale selection
    unsigned int ODR_XL:3; // Output data rate and power mode selection
} CTRL_REG6_XL_t;
    
/*
 * See Table 70 on page 53 for more detail
 */
typedef struct __attribute__((packed)) {
    unsigned int HPIS1:1; // High-pass filter enabled
    unsigned int NA1:1; // Not used, should be 0
    unsigned int FDS:1; // Filtered data selection
    unsigned int NA2:2; // Not used, should be 0
    unsigned int DCF:2; // Accelerometer digital filter (high pass and low pass) cutoff frequency selection
    unsigned int HR:1; // High resolution mode for accelerometer
} CTRL_REG7_XL_t;
    
/*
 * Structure that unites all the registers related to the accelerometer
 */
typedef struct __attribute__((packed)) {
    CTRL_REG5_XL_t CTRL_REG5_XL;
    CTRL_REG6_XL_t CTRL_REG6_XL;
    CTRL_REG7_XL_t CTRL_REG7_XL;
    uint8_t x;
} ControlRegisters_t;

/**
 * Map from scale to the sensitivity coefficients
 * These coefficients help to convert bare numbers to
 * acceleration in g units
 */
const float sensitivityCoeffs[] = { 0.000061, 0.000732, 0.000122, 0.000244 };

/**
 * Checks I2C connection with the device.
 * @return true in case of success
*/
bool Accel_test() {

    uint8_t response;
    LSM9DS1_readMemory(DEVICE_ADDR, WHO_I_AM_ADDR, &response, sizeof(response));
    return response == WHO_I_AM_VALUE;
}

/*
 * Writes registers of lsm9ds1 so that it is configured
 * according to the settings in config
 * @param config settings to be written to lsm9ds1
 */
void Accel_setup(Accel_Config_t* config) {

    ControlRegisters_t cr = {0};
    
    // Fill out CTRL_REG5_XL
    cr.CTRL_REG5_XL.DEC = config->decimation;
    cr.CTRL_REG5_XL.Xen_XL = config->xEnable ? 1 : 0;
    cr.CTRL_REG5_XL.Yen_XL = config->yEnable ? 1 : 0;
    cr.CTRL_REG5_XL.Zen_XL = config->zEnable ? 1 : 0;
    
    // Fill out CTRL_REG6_XL
    cr.CTRL_REG6_XL.ODR_XL = config->dataRate;
    cr.CTRL_REG6_XL.FS_XL = config->scale;
    cr.CTRL_REG6_XL.BW_SCAL_ODR = (config->cutoffFreq == ACCEL_CUTOFF_FREQ_DR) ? 0 : 1;
    cr.CTRL_REG6_XL.BW_XL = (config->cutoffFreq == ACCEL_CUTOFF_FREQ_DR) ? 0 : config->cutoffFreq;
    
    // Fill out CTRL_REG7_XL
    cr.CTRL_REG7_XL.HR = (config->digitalFilter == ACCEL_DFILTER_NONE) ? 0 : 1;
    cr.CTRL_REG7_XL.DCF = (config->digitalFilter == ACCEL_DFILTER_NONE) ? 0 : config->digitalFilter;
    
    // Write all registers related to the accelerometer at once
    // We can write them at once because they are located
    // contiguously starting from CTRL_REG5_XL
    LSM9DS1_writeMemory(DEVICE_ADDR, CTRL_REG5_XL_ADDR, (uint8_t*)&cr, sizeof(cr));

    ControlRegisters_t test = {0};
    LSM9DS1_readMemory(DEVICE_ADDR, CTRL_REG5_XL_ADDR, (uint8_t*)&test, sizeof(test));
}

/*
 * Reads acceleration for axes data from the accelerometer
 * @param config is required to properly address scaling
 * @param data contains properly scaled acceleration values
 */
void Accel_readData(Accel_Config_t* config, Accel_Data_t* data)
{
    // Temp storage for data
    int16_t rawData[3];

    // Read axis values from the device.
    // I am reading them in the reversed order to address lowendiannes of MSP430.
    // Doing that I can convert the bytes to array of 16-bit integers properly
    LSM9DS1_readMemoryReversed(DEVICE_ADDR, OUT_X_XL_ADDR, (uint8_t*)rawData, sizeof(rawData));

    // Convert 16-bit integer values to floats, taking into account scaling
    float sensitivity = sensitivityCoeffs[config->scale];
    data->x = rawData[2] * sensitivity;
    data->y = rawData[1] * sensitivity;
    data->z = rawData[0] * sensitivity;
}

/*
 * Checks if a new set of data can be read from the accelerometer
 * @return true if new data is available
 */
bool Accel_isDataAvailable() {

    uint8_t statusRegister;
    LSM9DS1_readMemory(DEVICE_ADDR, STATUS_REG_ADDR, &statusRegister, 1);
    return (statusRegister & XLDA_BIT_MASK) != 0;
}
