/*******************************************************************************
 * Copyright 2019 Igor Semenov (is0031@uah.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *******************************************************************************/
#pragma once
#include <stdbool.h>
#include "i2c.h"

/*
 * Data rate for reading values from the sensor
 */
typedef enum{
    ACCEL_DATA_RATE_PD = 0, // Power-down mode (accelerometer is not active)
    ACCEL_DATA_RATE_10,     // 10 Hz
    ACCEL_DATA_RATE_50,     // And so on ...
    ACCEL_DATA_RATE_119,
    ACCEL_DATA_RATE_238,
    ACCEL_DATA_RATE_476,
    ACCEL_DATA_RATE_952
} Accel_DataRate_t;

/*
 * Decimation (skipping samples) factor
 */
typedef enum{
    ACCEL_DECIMATION_NONE = 0, // No skipping
    ACCEL_DECIMATION_2,        // Keep every 2nd sample
    ACCEL_DECIMATION_4,        // And so on ...
    ACCEL_DECIMATION_8
} Accel_Decimation_t;

/*
 * Scale setting. Determines the maximum value
 * of acceleration in g that the accelerometer can measure.
 * The more this value the less accurate are the measurements
 * for small values of acceleration
 */
typedef enum{
    ACCEL_SCALE_2 = 0, // Max value that can be measured is 2g
    ACCEL_SCALE_16,    // And so on ...
    ACCEL_SCALE_4,
    ACCEL_SCALE_8,
} Accel_Scale_t;

/*
 * Anti-aliasing filter (analog) bandwidth selection
 */
typedef enum{
    ACCEL_CUTOFF_FREQ_DR = -1, // Determined by data rate
    ACCEL_CUTOFF_FREQ_408 = 0, // 408 Hz
    ACCEL_CUTOFF_FREQ_211,     // And so on ...
    ACCEL_CUTOFF_FREQ_105,
    ACCEL_CUTOFF_FREQ_50,
} Accel_CutoffFreq_t;

/*
 * Internal digital filter configuration
 * See table 71 of the datasheet
 */
typedef enum{
    ACCEL_DFILTER_NONE = -1, // Digital filter is disabled
    // The following modes enable the filter.
    ACCEL_DFILTER_50 = 0, // low-pass cutoff is data_rate/50
    ACCEL_DFILTER_100,    // low-pass cutoff is data_rate/100
    ACCEL_DFILTER_9,      // And so on ...
    ACCEL_DFILTER_400
} Accel_DigitalFilter_t;

/**
 * Keeps config for the accelerometer
 */
typedef struct {
    Accel_DataRate_t dataRate;           // See corresponding data structure for more detail
    Accel_Scale_t scale;                 // See corresponding data structure for more detail
    Accel_CutoffFreq_t cutoffFreq;       // See corresponding data structure for more detail
    Accel_Decimation_t decimation;       // See corresponding data structure for more detail
    Accel_DigitalFilter_t digitalFilter; // See corresponding data structure for more detail
    bool xEnable;                        // Enables X-axis
    bool yEnable;                        // Enables Y-axis
    bool zEnable;                        // Enables Z-axis
} Accel_Config_t;

/*
 * Keeps acceleration values for axes
 */
typedef struct __attribute__((packed)) {
    float x;
    float y;
    float z;
} Accel_Data_t;

void Accel_setup(Accel_Config_t* config);
void Accel_readData(Accel_Config_t* config, Accel_Data_t* data);
bool Accel_test();
bool Accel_isDataAvailable();
