# CPE325 Lab demos

This repository contains MSP430 assembly language and C/C++ programs used in Embedded Systems Laboratory (CPE325) course at the University of Alabama in Huntsville.

# Usage

Clone this repository by running the following command:
```
git clone https://gitlab.com/lacasa/cpe325-lab-demos.git
```

After that, you can create a workspace for Code Composer Studio by typing:
```
cd cpe325-lab-demos
python setup.py "C:\ti\ccsv8"
```
The path specified as the first argument can be different on your machine. Change it according to the real location of your Code Composer Studio. Make sure you use double quotes if your path contains backslash characters. After running the command, wait a few minutes while `setup.py` script will be creating projects for you.

Now you can open Code Composer Studio, choose the folder with cloned repository as your workspace, and try to run our demo projects.

If you want to remove all the stuff created by Code Composer Studio, run this command:
```
git clean -fxd
```

# Contributing

If you think that some part of the code can be rewritten or annotated to make it more intuitive for students, you are welcome to improve it. But before starting, read this section to learn how to properly do that.

## Do not modify code using GitLab web editor

If you do that, you will (1) commit code that was not tested and (2) produce countless commits with useless messages that contaminate commit history. Consider, instead, the following workflow:
- Clone the repository if you have not done it yet.
- Make all the changes that you think belong to one commit on your local machine.
- Test you changes (compile and run the code on the board).
- Commit your changes with a meaningful message. 
- Push your commit to the remote repository.

## Never change the history of `master` branch

This means that you may add new commits, but may not remove previous ones. Do no not forget that other people may also be working on this repository. If you change the history of `master` branch, you will cause a lot of problems to them. That is why it is also important to make sure that the commit you are submitting is valid and you will not want remove it later.

## Write meaningful commit messages

Messages like `Update Lab7_D6.c` make no sense, because the information like that can be obtained from the diff with the previous commit. Instead, try to describe how your commit helps to improve the repository.

Use imperative mood for your messages. That is, you need to tell git to do some action. For example: "Improve code readability by using constants" or "Make comments for UART more descriptive".

## Code convention
To provide consistency of our code base, we agreed to follow the set of rules describes below. Please read them before contributing. These rules can be extended or changed at any time if they become difficult to follow or if they are decided to be unnecessary. If you think that some part of the code does not follow these rules, feel free to fix it.

### Function names

Function names should be descriptive and start with a lower case letter and can be capitalized when required instead of using `_`. For example, it is better to use this name:
```
void sendCharacter(char c)
```
instead of these:
```
void send_character(char c);
...
void sendChar(char c)
...
void SendChar(char c)
```

If a few functions belong to one group, it is better to add some common prefix to their names:
```
char UART_readCharacter();
void UART_writeCharacter(char c);
...
char SPI_readByte();
void SPI_writeByte(char c);
```

### Magic numbers

[Magic numbers](https://en.wikipedia.org/wiki/Magic_number_(programming)) make code more difficult to understand. For that reason, when working with bits, consider using bit names:
```
P2DIR |= BIT0 | BIT1;
...
ADC12MCTL0 = INCH_3;
```
instead of bare numbers:
```
P2DIR |= 0x03;
...
ADC12MCTL0 = 3;
```

This also applies to assembly code. For example, you can write:
```
bis.b #BIT0|BIT1, &P2DIR
```
instead of:
```
bis.b #03h, &P2DIR
```

### Merging bitmasks

There are two ways of merging bitmasks: using `+` or using `|`. Even though `+` can be found in TI's code, [some developers](https://softwareengineering.stackexchange.com/questions/23852/bitwise-or-vs-adding-flags) consider it bad practice since `+` is (1) less intuitive and (2) more dangerous. So, it is preferable to use bitwise or (`|`) in expressions like `WDTCTL = WDTPW | WDTHOLD;` in our demo code.