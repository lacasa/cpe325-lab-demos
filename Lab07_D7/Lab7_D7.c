/*------------------------------------------------------------------------------
 * File:        Lab7_D6.c (CPE 325 Lab7 Demo code)
 * Function:    Buzzing the buzzer using Timer_B (MPS430FG4618)
 * Description: In this C program, Timer_B is configured for up mode with
 *              SMCLK source. In this mode timer TB counts the value from 0 up
 *              to value stored in TB0CCR0. So the counter period is CCR0*1us.
 *              The TB4 output signal is configured to toggle every time the
 *              counter reaches TB0CCR4. This generates the square wave at TB4
 *              with 1/2*CCRO MHz frequency. TB4 is multiplexed with the P3.5,
 *              and buzzer is connected to this output. Adjusting the TB0 CCR
 *              register values adjusts the frequency of the buzzer.
 * Clocks:      ACLK = LFXT1 = 32768Hz, MCLK = SMCLK = DCO = default (2^20 Hz)
 *              An external watch crystal between XIN & XOUT is required for ACLK
 *
 *                          MSP430xG461x
 *                      -------------------
 *                   /|\|              XIN|-
 *                    | |                 | 32kHz
 *                    --|RST          XOUT|-
 *                      |                 |
 *                      |         P3.5/TB4|--> Buzzer
 *                      |                 |
 * Input:       None
 * Output:      Buzzer buzzes at 100Hz frequency
 * Author:      Aleksandar Milenkovic, milenkovic@computer.org
 *              Mounika Ponugoti, mp0046@uah.edu
 *------------------------------------------------------------------------------*/
#include <msp430xG46x.h> 

void main(void) {
    WDTCTL = WDTPW + WDTHOLD;  // Stop WDT

    P3DIR |= BIT5;             // P3 Bit 5 set to output
    P3SEL |= BIT5;             // P3 BIT 5 set to TB4

    TB0CCTL4 = OUTMOD_4;       // TB0 output is in toggle mode 
    TB0CTL = TBSSEL_2 + MC_1;  // SMCLK is clock source, UP mode
    TB0CCR0 = 5000;
    TB0CCR4 = 1000;
    _BIS_SR(LPM0_bits);        // Enter Low Power Mode 0
}
