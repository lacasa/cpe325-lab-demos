/*------------------------------------------------------------------------------
 * File:        Lab3_D3.c (CPE 325 Lab3 Demo code)
 * Function:    Turning on LED1 when SW1 is pressed (MPS430FG4618)
 * Description: This C program turns on LED1 connected to P2.2 when the SW1 is
 *              pressed. SW1 is connected to P1.0 and when the switch is pressed
 *              it is logic 0 (check the schematic). To avoid faulty detection
 *              of switch press delay of 20ms is added before turning on the LED1.
 * Clocks:      ACLK = 32.768kHz, MCLK = SMCLK = default DCO (~1 MHz)
 *
 *                           MSP430xG461x
 *                       -----------------
 *                   /|\|                 |
 *                    | |                 |
 *                    --|RST              |
 *                      |             P2.2|-->LED1(GREEN)
 *                      |             P1.0|<-- SW1
 *                      |                 |
 * Input:       Press SW1
 * Output:      LED1 is turned on when SW1 is pressed
 * Authors:     Aleksandar Milenkovic, milenkovic@computer.org
 *              Mounika Ponugoti, mp0046@uah.edu
 *------------------------------------------------------------------------------*/
#include <msp430xG46x.h>

#define SW1_PRESSED ((P1IN&BIT0)==0)

void main(void)
{
    WDTCTL = WDTPW + WDTHOLD;              // Stop watchdog timer
    P2DIR |= BIT2;                         // Set P2.1 to output direction (0000_0100)
    P2OUT &= ~BIT2;                        // LED1 is OFF
    int i = 0;
    for (;;) {                             // Infinite loop
        if (SW1_PRESSED) {                 // If SW1 is pressed
            for (i = 0; i < 2000; i++);    // Debounce ~20 ms
            if (SW1_PRESSED) P2OUT |= BIT2;// SW1 pressed, turn LED1 on
            while (SW1_PRESSED);           // wait while SW1 is pressed
            P2OUT &= ~0x04;                // Turn LED1 off 
        }
    }
}
