/*------------------------------------------------------------------------------
 * File:        Lab3_D1.c (CPE 325 Lab3 Demo code)
 * Function:    Turning on LED1(MPS430FG4618)
 * Description: This C program turns on LED1 connected to P2.2 by writing 1
 *              (P2.2 = 1).
 * Clocks:      ACLK = 32.768kHz, MCLK = SMCLK = default DCO (~1 MHz)
 *
 *                           MSP430xG461x
 *                       -----------------
 *                   /|\|                 |
 *                    | |                 |
 *                    --|RST              |
 *                      |             P2.2|-->LED1(GREEN)
 *                      |                 |
 * Input:       None
 * Output:      LED1 is turned on
 * Author:      Aleksandar Milenkovic, milenkovic@computer.org
 *              Mounika Ponugoti, mp0046@uah.edu
 *------------------------------------------------------------------------------*/
#include  <msp430xG46x.h>

void main(void)
{
    WDTCTL = WDTPW + WDTHOLD;   // Stop watchdog timer
    P2DIR |= 0x04;              // Set P2.2 to output direction (0000_0100)
    P2OUT |= 0x04;              // Set P2OUT to 0000_0100b (LED1 is ON)
    for (;;);                   // Infinite loop
}
