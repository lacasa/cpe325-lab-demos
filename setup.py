#!/usr/bin/env python
"""Creates Code Composer Studio projects from folders containing 'setup-config' file"""
__author__ = "Igor Semenov (is0031@uah.edu)"

import subprocess
import configparser
import os
import sys
import argparse
from collections import OrderedDict

class CssProject:

    def __init__(self, exe_path, project_name, workspace_path):
        self.exe_path = exe_path
        self.creation_options = OrderedDict()
        self.build_options = OrderedDict()
        self.source_files = OrderedDict()

        self.creation_options['noSplash'] = ''
        self.creation_options['application'] = 'com.ti.ccstudio.apps.projectCreate'
        self.creation_options['data'] = workspace_path # set workspace to add new projects
        self.creation_options['ccs.name'] = project_name # set project name
        self.creation_options['ccs.overwrite'] = 'keep' # do not overwrite files in project folder

    def add_creation_option(self, key, value):
        self.creation_options.add(key, value)
        
    def add_build_option(self, key, value):
        self.build_options.add(key, value)
        
    def apply_config(self, config):
        if 'creation-options' in config:
            self.creation_options.update(config['creation-options'])
            
        if 'build-options' in config:
            self.build_options.update(config['build-options'])
        
    def link_source_file(self, file_name):
        self.source_files[file_name] = None
    
    def create(self, timeout=None):
        
        # Exe file of CCS is the first element of the argument list 
        ccs_args = [self.exe_path]
        
        # Add project creation options to the argument list 
        for k, v in self.creation_options.items():
            ccs_args.extend(['-' + k] + ([] if v == '' else [v]))
        
        # Add project build options to the argument list
        for k, v in self.build_options.items():
            ccs_args.extend(['-ccs.setBuildOption', k, v])
            
        # Add source files to be linked to the argument list    
        for k in self.source_files:
            ccs_args.extend(['-ccs.linkFile', k]) 
        
        # Run Code Composer Studio
        try:
            subprocess.call(ccs_args, timeout=timeout)
        except subprocess.TimeoutExpired:
            print('Killed by Python, but your project is probably OK.', end='\n', flush=True)
        
# Parse input arguments
argparser = argparse.ArgumentParser(description='Creates Code Composer Studio projects.')
argparser.add_argument('css_path', help='Path to Code Composer Studio. Ex: C:/ti/ccsv8/')
args = argparser.parse_args()
     
# Determine path to CCS executable     
ccs_exe = os.path.join(args.css_path, 'eclipse/eclipsec.exe')

# Sometimes CCS hangs after creating a project.
# I do not know if it is a bug in the IDE or
# if this behavior is caused by calling it from Python improperly.
# So, I just give ccs_timeout seconds to CCS and kill it after.
# Fix it if you know a better way to solve the problem.
ccs_timeout = 40
        
# Workspace will be created in the current directory
workspace_path = os.getcwd()

# These file types will be added to the projects
src_extensions = ['.c', '.asm']

# Load config that affects all projects
global_config = configparser.ConfigParser()
global_config.optionxform = str
global_config.read('setup-config')

# Iterate through all the items in the workspace   
for project_dir in os.scandir(workspace_path):
    
    # Choose folders only
    if not project_dir.is_dir():
        continue
        
    # Only folders containing 'setup-config' will be turned into projects
    local_config_path = os.path.join(project_dir.path, 'setup-config')
    if not os.path.isfile(local_config_path):
        continue
    
    # To improve performance when only a couple projects need to be created,
    # we skip running CSS for existing projects by looking for .project file
    project_file = os.path.join(project_dir.path, '.project')
    if os.path.isfile(project_file):
        print(f'Folder {project_dir.name} skipped because it contains an existing project', end='\n', flush=True)
        continue

    ccs_project = CssProject(ccs_exe, project_dir.name, workspace_path)

    # Link source files found in the project directory
    # Note: It turned out that we do not need to manually add sources to
    # the project. It is enough that they are located in the project's folder
    '''for src_file in os.scandir(project_dir.path):
        if src_file.path.endswith(tuple(src_extensions)):
            ccs_project.link_source_file(src_file.name)'''
    
    # Apply global config first so that its fields can be overridden
    ccs_project.apply_config(global_config)
    
    # Load and apply config that affects only this project
    local_config = configparser.ConfigParser()
    local_config.optionxform = str
    local_config.read(local_config_path)
    ccs_project.apply_config(local_config)
    
    # Empty assembly projects are created with default main.asm file 
    # (Why do you call them empty, TI? Why?)
    # Since we don't need this file, we could get rid of it.
    # However, our projects may also contain main.asm.
    # So, we check if this file existed before project creation,
    # if it did not, we delete main.asm after.
    main_asm_path = os.path.join(project_dir.path, 'main.asm')
    main_asm_existed = os.path.isfile(main_asm_path)
    
    # Run CCS for project creation
    ccs_project.create(timeout=ccs_timeout)
    
    # Delete main.asm if it did not exist before project creation
    if not main_asm_existed and os.path.isfile(main_asm_path):
        os.remove(main_asm_path)