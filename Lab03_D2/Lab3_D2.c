/*------------------------------------------------------------------------------
 * File:        Lab3_D2.c (CPE 325 Lab3 Demo code)
 * Function:    Blinking LED1 and LED2 (MPS430FG4618)
 * Description: This C program toggle LED1 and LED2 at 1Hz by xoring P2.1 and
 *              P2.2 inside a loop. The LEDs are on when P2.1=1 and P2.2=1.
 *              The LED1 is initialized to be off and LED2 to be on.
 * Clocks:      ACLK = 32.768kHz, MCLK = SMCLK = default DCO (~1 MHz)
 *
 *                           MSP430xG461x
 *                       -----------------
 *                   /|\|                 |
 *                    | |                 |
 *                    --|RST              |
 *                      |             P2.2|-->LED1(GREEN)
 *                      |             P2.1|-->LED2(YELLOW)
 *                      |                 |
 * Input:       None
 * Output:      LED1 and LED2 blinks alternately at 1Hz frequency
 * Author:      Aleksandar Milenkovic, milenkovic@computer.org
 *              Mounika Ponugoti, mp0046@uah.edu
 *------------------------------------------------------------------------------*/
#include <msp430xG46x.h>

void main(void)
{
    WDTCTL = WDTPW + WDTHOLD;   // Stop watchdog timer
    P2DIR |= BIT2+BIT1;         // Set P2.1 and P2.2 to output direction (0000_0110)
    P2OUT &= ~BIT1;             // LED2 is OFF
    P2OUT |= BIT2;              // LED1 is ON
    unsigned int i = 0;
    while(1){                   // Infinite loop
        for (i = 0; i < 50000; i++); // Delay 0.5s
                                // 0.5s on, 0.5s off => 1/(1s) = 1Hz
        P2OUT ^= (BIT1+BIT2);   // Toggle LED1 and LED2
    }
}
