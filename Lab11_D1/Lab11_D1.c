/******************************************************************************
 File:          Lab11_D1.c
 Function:      Blink Led1 and Led2
 Description:   This program blinks Led1 and Led2 in an infinite loop

 Clocks:        ACLK = LFXT1 = 32768Hz, MCLK = SMCLK = default DCO = 32 x ACLK = 1048576Hz

      MSP430xG461x
     -----------------
 /|\ |            XIN|-
  |  |               | 32kHz
  |--|RST        XOUT|-
     |               |
     |           P2.1|- Led2
     |           P2.2|- Led1
     |               |

 Input:     None
 Authors:   A. Milenkovic,
******************************************************************************/
#include <msp430.h> 

int main(void) {
    WDTCTL = WDTPW + WDTHOLD;   // stop watchdog timer
    P2DIR |= (BIT1 + BIT2);
    P2OUT = 0x02;
    unsigned int i = 0;
    for(;;) {
        P2OUT ^= (BIT1 + BIT2);
        for(i = 0; i < 5000; i++);
    }

    return 0;
}
