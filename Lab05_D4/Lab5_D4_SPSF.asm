;-------------------------------------------------------------------------------
; File       : Lab5_D4_SPSF.asm (CPE 325 Lab5 Demo code)
; Function   : Finds a sum of an input integer array
; Description: suma_spsf is a subroutine that sums elements of an integer array.
;              The subroutine allocates local variables on the stack:
;                  counter (SFP+2)
;                  sum (SFP+4)
; Input      : The input parameters are on the stack pushed as follows:
;                  starting address of the array
;                  array length
;                  display id
; Output     : No output
; Author     : A. Milenkovic, milenkovic@computer.org
; Date       : September 14, 2008
;------------------------------------------------------------------------------
           .cdecls C,LIST,"msp430.h"       ; Include device header file

           .def    suma_spsf

            .text
suma_spsf:
        ; save the registers on the stack
        push    R12              ; save R12 - R12 is stack frame pointer
        mov.w   SP, R12          ; R12 points on the bottom of the stack frame
        sub.w   #4, SP           ; allocate 4 bytes for local variables
        push    R4               ; pointer register
        clr.w   -4(R12)          ; clear sum, sum=0
        mov.w    6(R12), -2(R12) ; get array length
        mov.w   8(R12), R4       ; R4 points to the array starting address
lnext:  add.w   @R4+, -4(R12)    ; add next element
        dec.w   -2(R12)          ; decrement counter
        jnz     lnext            ; repeat if not done
        bit.w   #1, 4(R12)       ; test display id
        jnz     lp34             ; jump to lp34 if display id = 1
        mov.b   -4(R12), P1OUT   ; lower 8 bits of the sum to P1OUT
        mov.b   -3(R12), P2OUT   ; upper 8 bits of the sume to P2OUT
        jmp     lend             ; skip to lend
lp34:   mov.b   -4(R12), P3OUT   ; lower 8 bits of the sum to P3OUT
        mov.b   -3(R12), P4OUT   ; upper 8 bits of the sume to P4OUT
lend:   pop     R4               ; restore R4
        add.w   #4, SP           ; collapse the stack frame
        pop     R12              ; restore stack frame pointer
        ret                      ; return
        .end
